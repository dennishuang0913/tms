function setEvent(vModal){
	var oFrom = vModal.find('select.oFrom');
	var oTo = vModal.find('select.oTo');
	var bFrom = vModal.find('button.cFrom');
	var bFromAll = vModal.find('button.cFromAll');
	var bTo = vModal.find('button.cTo');
	var bToAll = vModal.find('button.cToAll');
	var bSave = vModal.find('button.cSave');
	var bCancel = vModal.find('button.cCancel');
	var bView = $('#'+vModal.attr('id')+'Btn');
	bFrom.click(function(){
		fnMoveItems(oFrom,oTo);
	})
	bFromAll.click(function(){
		fnMoveAllItems(oFrom,oTo);
	});
	bTo.click(function(){
		fnMoveItems(oTo,oFrom);
	});
	bToAll.click(function(){
		fnMoveAllItems(oTo,oFrom);
	});
	bSave.click(function(){
		var tContainer = $('#'+vModal.attr('modal-to')).DataTable();
		tContainer.rows().remove().draw(false);
		oTo.children('option').each(function() {
			tContainer.rows.add([ [ $(this).val(), $(this).text(), ] ]).draw(false);
		});
	});
	
	bView.click(function(){
		vModal.modal('show');
	});
}

function setModalContainerOption(vModal, jAll) {
	//var vModal = $('#' + iDiv);
	var oFrom = vModal.find('select.oFrom');
	var oTo = vModal.find('select.oTo');
	oFrom.children('option').remove();
	oTo.children('option').remove()
	var oContainer = $('#' + vModal.attr('modal-to'));
	var tContainer = $('#' + vModal.attr('modal-to')).DataTable();
	var newOption = null;
	$.each(jAll, function(key, val) {
		newOption = new Option(val, key, false, false);
		if (isUndefined(oContainer.find('tbody td').filter(function() { return $.text([this]) == key; }).html()) == false) {
			oTo.append(newOption);
		} else {
			oFrom.append(newOption);
		}
	});
}

function getContainerVal(iTable){
	var vTable = $('#' + iTable);
	var vTds = vTable.find('tr td');
	var jObj = {};
	for (var i=0; i<vTds.length; i++){
		jObj[ $(vTds[i]).html() ]=$(vTds[i+1]).html();
		i++;
	}
	alert(jObj);
}

function fnMoveItems(oFrom, oTo) {
	if ((oFrom != null) && (oTo != null)) {
		if (oFrom.children('option').length < 1) {
			alert('已無項目');
			return false;
		}
		// when no Item is selected the index will be -1
		if (oFrom.children('option:selected').length < 1) {
			alert('請先選取項目');
			return false;
		}
		
		oFrom.children('option:selected').each(function() {
			var newOption = new Option(); // Create a new instance of ListItem
			newOption.text = $(this).text();
			newOption.value = $(this).val();
			oTo.append( newOption); 
			$(this).remove();
		});
	}
	return false;
}

function fnMoveAllItems(oFrom, oTo) {
	if ((oFrom != null) && (oTo != null)) {
		if (oFrom.children('option').length < 1) {
			alert('已無項目');
			return false;
		}
		oFrom.children('option').each(function() {
			var newOption = new Option(); // Create a new instance of ListItem
			newOption.text = $(this).text();
			newOption.value = $(this).val();
			oTo.append( newOption); 
			$(this).remove();
		});
	}
	return false;
}