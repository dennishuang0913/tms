var driverId = "";

function initLayout(vActive) {
	// >> 讀取共用頁面
	var pm = $.get("menu.html", function(data) {
		$('aside.main-sidebar').html(data);
		// $('li.treeview:eq(0)').addClass('active');
	});
	var ph = $.get("header.html", function(data) {
		$('header.main-header').html(data);
	});
	var ps = $.get("side.html", function(data) {
		$('aside.control-sidebar').html(data);
	});
	$.when(pm, ph, ps).then(function() {
		$.getScript("resources/js/app.min.js");
		$.getScript("resources/js/demo.js");
		if (vActive != null) {
			var vObj = $('aside.main-sidebar').find('a[href*="' + vActive + '"]').parent('li');
			vObj.addClass('active');
			vObj.parent('ul').parent('li.treeview').addClass('active');
		}
	});
}

function getUrlParameter(sParam) {
	var sPageURL = decodeURIComponent(window.location.search.substring(1)), sURLVariables = sPageURL.split('&'), sParameterName, i;

	for (i = 0; i < sURLVariables.length; i++) {
		sParameterName = sURLVariables[i].split('=');

		if (sParameterName[0] === sParam) {
			return sParameterName[1] === undefined ? true : sParameterName[1];
		}
	}
};

function isUndefined(obj) {
	return (typeof (obj) == "undefined" ? true : false);
}

//dataTable
function initDataTable(formId) {
	var vTable = $('#' + formId).DataTable({
		"paging" : true,
		"ordering" : true,
		"info" : true,
		"autoWidth" : true,
		"language" : {
			"url" : "/resources/plugins/datatables/Chinese-traditional.json"
		},
		"initComplete" : function(settings, json) {
			$('div.dataTables_filter').addClass('pull-right');
			$('div.dataTables_paginate').addClass('pull-right');
		}
	});
}

function initDataTableScrollY(formId, height) {
	var vThead = $('#' + formId + ' thead');
	var vTable = $('#' + formId).DataTable({
		paging : false,
		ordering : false,
		searching : false,
		info : false,
		autoWidth : true,
		scrollY : 60,
		scrollCollapse : false,
		scroller : true,
		language : {
			"url" : "/resources/plugins/datatables/Chinese-traditional.json"
		},
		initComplete : function(settings, json) {
			$('div.dataTables_filter').addClass('pull-right');
			$('div.dataTables_paginate').addClass('pull-right');
			// bug for datatable, not auto width with thead
			$('#' + formId).parent('div').height(height);
			vThead.parent('table').width('100%').height(height);
			vThead.parent('table').parent('div').width('100%');
			vThead.parent('table').parent('div').parent('div').find('table').css('margin-bottom', 0);
		}
	});

	/*
	 * vTable.on( 'column-sizing', function () { alert( 'column-sizing' ); } );
	 * vTable.on( 'responsive-resize', function () { alert( 'Table redrawn' ); } );
	 * vTable.on( 'draw', function () { alert( 'draw' ); } ); vTable.on(
	 * 'redraw', function () { alert( 'redraw' ); } ); vTable.on( 'init',
	 * function () { alert( 'init' ); } );
	 */
	//vTable.rows.add([ [ "1", "Armand", ] ]);
}

function  setDataTableDisplayLength(formId, length){
	var iList = $('#'+formId).dataTable();
	iList.fnSettings()._iDisplayLength = length;
	iList.fnDraw();
}

//datePicker
function initDatePickerExt() {
	$('.cDatepicker').datepicker({
		autoclose : true
	});

	$(".fa-calendar").parent('div').click(function() {
		var obj = $(this).parent('div').find('input.cDatepicker');
		if( !obj.is(':disabled')){
			$(this).parent('div').find('input.cDatepicker').datepicker('show');
		} 
	});
}

// pic
function initPic() {
	$('#iPicLook').click(function() {
		$('#iUplPic').click();
	});

	$('#iPicRemove').click(function() {
		$('#iPic').attr('src', '');
	});

	function preview(input) {
		if (input.files && input.files[0]) {
			var reader = new FileReader();
			reader.onload = function(e) {
				$('#iPic').attr('src', e.target.result);
			}
			reader.readAsDataURL(input.files[0]);
		}
	}
	$("body").on("change", "#iUplPic", function() {
		preview(this);
	})
}


// modal
function initModalContainer(iDiv, jAll) {
	var vModal = $('#' + iDiv);
	if (vModal != null) {
		vModal.addClass('modal').addClass("fade");
		vModal.attr('tabindex', "-1");
		vModal.attr('role', "dialog");
		var modal = $.get("modalContainer.html", function(data) {
			vModal.html(data);
		});
		var modalJs = $.getScript("resources/js/modalContainer.js");
		$.when(modal, modalJs).then(function() {
			setEvent(vModal);
			vModal.find('h4.modal-title').html(vModal.attr('title'));
			setModalContainerOption(vModal, jAll);
		});
	}
}

// validate
function checkTWID(id) {
	// 建立字母分數陣列(A~Z)
	var city = new Array(1, 10, 19, 28, 37, 46, 55, 64, 39, 73, 82, 2, 11, 20, 48, 29, 38, 47, 56, 65, 74, 83, 21, 3, 12, 30)
	id = id.toUpperCase();
	// 使用「正規表達式」檢驗格式
	if (id.search(/^[A-Z](1|2)\d{8}$/i) == -1) {
		return false;
	} else {
		// 將字串分割為陣列(IE必需這麼做才不會出錯)
		id = id.split('');
		// 計算總分
		var total = city[id[0].charCodeAt(0) - 65];
		for (var i = 1; i <= 8; i++) {
			total += eval(id[i]) * (9 - i);
		}
		// 補上檢查碼(最後一碼)
		total += eval(id[9]);
		// 檢查比對碼(餘數應為0);
		return ((total % 10 == 0));
	}
}

function checkAddrVillage(vAddr){
	var vTmp = vAddr.slice(-1);
	if(vTmp == '里' || vTmp == '村') {
		return true;
	} else {
		return false;
	}
}

// address
function setAddressEvent(cAddr, iAddr){
	$("."+cAddr).change(function() {
		var vAddr = "";
		var iCnt = 0;
		$("."+cAddr).each(function() {
			if($(this).val() != ''){
				switch (iCnt) {
				case 0: vAddr = vAddr + $(this).val(); break;
				case 1: vAddr = vAddr + $(this).val(); break;
				case 2:
					var vTmp = $(this).val().slice(-1);
					if (vTmp == '里' || vTmp == '村') {
						vAddr = vAddr + $(this).val();
					}
					break;
				case 3: vAddr = vAddr + $(this).val() + "鄰"; break;
				case 4:
					var vTmp = $(this).val().slice(-1);
					if (vTmp == '路' || vTmp == '街') {
						vAddr = vAddr + $(this).val();
					}
					break;
				case 5: vAddr = vAddr + $(this).val() + "段"; break;
				case 6: vAddr = vAddr + $(this).val() + "巷"; break;
				case 7: vAddr = vAddr + $(this).val() + "弄"; break;
				case 8: vAddr = vAddr + $(this).val() + "號"; break;
				case 9: vAddr = vAddr + "之" + $(this).val() +"&nbsp;&nbsp;&nbsp;"; break;
				case 10: vAddr = vAddr + $(this).val() + "樓"; break;
				case 11: vAddr = vAddr + $(this).val() + "室"; break;
				}
			}
			iCnt++;
		});
		$('#'+iAddr).html(vAddr);
	});
}